import React from 'react';
import ContactForm from './ContactForm';
import { connect } from 'react-redux';
import { addContact } from "../actions/Contacts";

const AddContact = (props)=>(
  <div>
      <h3>Contact:</h3>
      <ContactForm onSubmitContact={(contact)=>{
          props.dispatch(addContact(contact));
          props.history.push('/');
      }}/>
  </div>
);

export default connect ()(AddContact);