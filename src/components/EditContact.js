import React from 'react';
import ContactForm from './ContactForm';
import { connect } from 'react-redux';
import { editContact } from '../actions/Contacts';

const EditContact = (props) => (
    <div className='container__box'>
        <ContactForm
            contact={props.contact}
            onSubmitContact={(contact) => {
                props.dispatch(editContact(props.contact.id, contact));
                props.history.push('/');
            }}
        />
    </div>
);

const mapStateToProps = (state, props) => {
    return {
        book: state.find((contact) =>
            contact.id === props.match.params.id)
    };
};

export default connect(mapStateToProps)(EditContact);