import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {removeContact} from '../actions/Contacts';
import {Card, CardContent,Typography,CardActions,IconButton} from '@material-ui/core';
import {Delete,Settings} from '@material-ui/icons';

const Contact = ({id,firstname,surname,personalcode,phone,dispatch})=>(
    <div>
        <Card>
            <CardContent>
                <Typography gutterBottom>
                    Firstname: {firstname}
                    Surname: {surname}
                </Typography>
                <CardActions>
                    <Link to={`/contact/${id}`}>
                    <IconButton>
                        <Settings/>
                    </IconButton>
                    </Link>
                    <IconButton onClick={()=>{
                        dispatch(removeContact({id}));
                    }}>
                        <Delete/>
                    </IconButton>
                </CardActions>
            </CardContent>
        </Card>
    </div>
);

export default connect()(Contact);