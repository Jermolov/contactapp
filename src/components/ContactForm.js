import React from 'react';

export default class ContactForm extends React.Component{
    constructor(props){
        super(props);
        this.onFirstnameChange = this.onFirstnameChange.bind(this);
        this.onSurnameChange = this.onSurnameChange.bind(this);
        this.onPersonalcodeChange = this.onPersonalcodeChange.bind(this);
        this.onPhoneChange = this.onPhoneChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    this.state={
        firstname:props.contact ? props.contact.firstname: '',
        surname:props.contact ? props.contact.surname: '',
        personalcode:props.contact ? props.contact.personalcode: 0 ,
        phone:props.contact ? props.contact.phone: 0 ,
        error:''
    };
}

onFirstnameChange(e){
    const firstname = e.target.value;
    this.setState(()=>({firstname:firstname}));
}

onSurnameChange(e){
    const surname = e.target.value;
    this.setState(()=>({surname:surname}));
}

onPersonalcodeChange(e){
    const personalcode = parseInt(e.target.value);
    this.setState(()=>({personalcode:personalcode}));
}

onPhoneChange(e){
    const phone = parseInt(e.target.value);
    this.setState(()=>({phone:phone}));
}

onSubmit(e){
    e.preventDefault();

    if (!this.state.firstname || !this.state.surname){
        this.setState(()=>({error: 'Please set firstname and surname'}));
    } else {
        this.setState(()=>({error:''}));
        this.props.onSubmitContact({
            firstname: this.state.firstname,
            surname:this.state.surname,
            personalcode:this.state.personalcode,
            phone:this.state.phone
        });
    }
}

render() {
    return (
        <div>
            {this.state.error && <p className='error'>{this.state.error}</p>}
            <form onSubmit={this.onSubmit} className='add-contact-form'>
                <input type="text" placeholder="Firstname"
                       autoFocus
                       value={this.state.firstname}
                       onChange={this.onFirstnameChange}
                />
                <br />

                <input type="text" placeholder="Surname"
                       value={this.state.surname}
                       onChange={this.onSurnameChange}
                />
                <br />

                <input type="text" placeholder="Personalcode"
                       value={this.state.personalcode}
                       onChange={this.onPersonalcodeChange}
                />
                <br />

                <input type="text" placeholder="Phone"
                       value={this.state.phone}
                       onChange={this.onPhoneChange}
                />
                <button>Add Contact</button>
            </form>
        </div>
        )
    }
}

