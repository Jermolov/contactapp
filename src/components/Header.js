import React from 'react';
import {NavLink} from 'react-router-dom';

const Header =()=>(
    <header>
        <h2>Contact Application</h2>
        <div>
            <NavLink to='/' activeClassName='activeNav' exact={true}>DashBoard</NavLink>
            <NavLink to='/add' activeClassName='activeNav'>Add Contact</NavLink>
        </div>
    </header>
);

export default Header;