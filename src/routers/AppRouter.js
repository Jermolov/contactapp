import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from '../components/Header';
import DashBoard from '../components/DashBoard';
import EditContact from '../components/EditContact';
import NotFound from '../components/NotFound';
import AddContact from "../components/AddContact";

const AppRouter =()=>(
    <BrowserRouter>
        <div>
            <Header/>
            <Switch>
                <Route path="/" component={DashBoard} exact={true}/>
                <Route path="/add" component={AddContact}/>
                <Route path="/contact/:id" component={EditContact}/>
                <Route component={NotFound}/>
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;