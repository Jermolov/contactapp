import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import getAppStore from './store/Store';
import {getContacts} from "./actions/Contacts";
import {Provider} from 'react-redux';

const store = getAppStore();

const template = (
    <Provider store={store}>
        <AppRouter/>
    </Provider>
);

store.dispatch(getContacts()).then(()=>{
   ReactDOM.render(template,document.getElementById('app'));
});