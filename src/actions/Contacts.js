import axios from '../axios/axios';

const _addContact = (contact) => ({
    type: 'ADD_CONTACT',
    contact
});

export const addContact= (contactData = {
    firstname: '',
    surname: '',
    personalcode: 0,
    phone: 0
}) => {
    return (dispatch) => {
        const contact = {
            firstname: contactData.firstname,
            surname: contactData.surname,
            personalcode: contactData.personalcode,
            phone: contactData.phone
        };

        return axios.post('contacts/create', contact).then(result => {
            dispatch(_addContact(result.data));
        });
    };
};

const _removeContact = ({ id } = {}) => ({
    type: 'REMOVE_CONTACT',
    id
});

export const removeContact = ({ id } = {}) => {
    return (dispatch) => {
        return axios.delete(`contacts/${id}`).then(() => {
            dispatch(_removeContact({ id }));
        })
    }
};

const _editContact = (id, updates) => ({
    type: 'EDIT_CONTACT',
    id,
    updates
});

export const editContact = (id, updates) => {
    return (dispatch) => {
        return axios.put(`contacts/${id}`, updates).then(() => {
            dispatch(_editContact(id, updates));
        });
    }
};

const _getContacts = (contacts) => ({
    type: 'GET_CONTACTS',
    contacts
});

export const getContacts= () => {
    return (dispatch) => {
        return axios.get('contacts').then(result => {
            const contacts = [];

            result.data.forEach(item => {
                contacts.push(item);
            });

            dispatch(_getContacts(contacts));
        });
    };
};