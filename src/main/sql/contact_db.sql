DROP TABLE IF EXISTS contact CASCADE;

CREATE TABLE contact(
  id BIGSERIAL PRIMARY KEY,
  firstname VARCHAR (128),
  surname VARCHAR (128),
  personalcode INT,
  phone INT
);