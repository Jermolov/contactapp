package com.example.ContactApp;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table(name = "contact")
@ToString
public class Contact {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "surname")
    private String surname;

    @Column(name = "personalcode")
    private Integer personalcode;

    @Column(name = "phone")
    private Integer phone;

    public Contact() {
    }

    public Contact(String firstname, String surname, Integer personalcode, Integer phone) {
        this.firstname = firstname;
        this.surname = surname;
        this.personalcode = personalcode;
        this.phone = phone;
    }
}
