package com.example.ContactApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class ContactController {
    @Autowired
    ContactRepository contactRepository;

    @GetMapping("/contacts")
    public List<Contact>getAllContacts(){
        List<Contact>list = new ArrayList<>();
        Iterable<Contact>contacts = contactRepository.findAll();
        contacts.forEach(list::add);
        return list;
    }

    @PostMapping("/contacts/create")
    public Contact createContact(@Valid @RequestBody Contact contact){
        return contactRepository.save(contact);
    }

    @GetMapping("/contacts/{id}")
    public ResponseEntity<Contact>getContact(@PathVariable("id")Long id){
        Optional<Contact>contactData=contactRepository.findById(id);
        if(contactData.isPresent()){
            return new ResponseEntity<>(contactData.get(), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/contacts/{id}")
    public ResponseEntity<Contact>updateContact(@PathVariable("id")Long id,@RequestBody Contact contact){
        Optional<Contact>contactData=contactRepository.findById(id);
        if(contactData.isPresent()){
            Contact savedContact = contactData.get();
            savedContact.setFirstname(contact.getFirstname());
            savedContact.setSurname(contact.getSurname());
            savedContact.setPersonalcode(contact.getPersonalcode());
            savedContact.setPhone(contact.getPhone());
            Contact updatedContact = contactRepository.save(savedContact);
            return new ResponseEntity<>(updatedContact,HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/contacts/{id}")
    public ResponseEntity<String>deleteContact(@PathVariable("id")Long id){
        try{
            contactRepository.deleteById(id);
        }catch (Exception e){
            return new ResponseEntity<>("Cant delete this contact!",HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>("Contact was deleted!",HttpStatus.OK);
    }
}
